from django.shortcuts import redirect, render
import json
from django.http import HttpResponse, JsonResponse
from django.views.generic import ListView
from info.models import datacovid
import requests


def datalist(request):
    return render(request,'datacov.html')

def search(request):
    find = request.GET['key']
    element2 = datacovid.objects.filter(provinsi__contains=find[:-1])
    response = element2.values()
    return JsonResponse(list(response), safe=False)

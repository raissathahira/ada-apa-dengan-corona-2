from django.test import TestCase, Client
from django.urls import resolve
from .views import datalist, search
from .models import datacovid
# Create your tests here.

class TestData(TestCase):
    def test_event_url_is_exist(self):
        response = Client().get('/DataCovid')
        self.assertEqual(response.status_code, 200)

    def test_event_index_func(self):
        found = resolve('/DataCovid')
        self.assertEqual(found.func, datalist)

    def test_event_using_template(self):
        response = Client().get('/DataCovid')
        self.assertTemplateUsed(response, 'datacov.html')

    def test_table_using_right_view_in_html(self):
        response = Client().get("/DataCovid")
        isi_html = response.content.decode("utf8")
        self.assertIn('<table class="table" id="dataTable" width="100%" cellspacing="0">', isi_html)

    def test_url_getdata_is_exist(self):
        response = Client().get("/getData?key=Riau")
        self.assertEquals(200, response.status_code)


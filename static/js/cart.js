// $(document).ready(function() {

//     $.ajax({
//         method : 'GET',
//         url : "/cari-produk",
//         success : function(hasil) {
//             console.log(hasil.items)
            
//             var result = "";
//             for (let i = 0; i < hasil.length; i++) {
//                 result += ('<div class="card p-2" style="width: 21rem; height: max-content;">')
//                 result += ('<img src="{{ hasil[i].fields.imageURL }}" class="card-img-top p-4" alt="{{product.name}}">')
//                 result += ('<div class="card-body">')
//                 result += ('<h5 class="card-title">' + hasil[i].fields.name + '</h5>')
//                 result += ('<p class="card-text">' + hasil[i].fields.desc + '</p>')
//                 result += ('<button data-product=' + hasil[i].fields.id + ' data-action="add" class="btn add-btn update-cart">Masukkan Keranjang</button>')
//                 result += (' <h5 class="pt-2 float-right harga">Rp' + hasil[i].fields.price+'</h5>')

//             }
//             $('#result').append(result);
//             $('#result').append("</div>");
//             $('#result').append("</div>");

//         }
//     })

//     $("#keyword").keyup(function(e) {
//         if (e.keyCode === 13) {
//             e.preventDefault();
//             $("#search").click();
//         }
//     });

//     $("#search").click(function() {
//         var book = $("#keyword").val();
//         console.log(book);
//         var books_url = "https://www.googleapis.com/books/v1/volumes?q=" + book;
//         console.log(books_url);

//         $.ajax({
//             method : 'GET',
//             url : "/cari-produk",
//             success : function(hasil) {
//                 console.log(hasil.items)ms)
//                 $("#hasil").empty();
                
//                 var result = "";
//                 for (let i = 0; i < hasil.items.length; i++) {
//                     result += ('<tr>' + '<td> <a id="judul" href="' + hasil.items[i].volumeInfo.canonicalVolumeLink + '">' + hasil.items[i].volumeInfo.title + ' </a> </td>' + '<td>' + hasil.items[i].volumeInfo.authors + '</td>' + '<td>')

//                     if (hasil.items[i].volumeInfo.description != undefined) {
//                         if (hasil.items[i].volumeInfo.description.length > 230) {
//                             result += hasil.items[i].volumeInfo.description.substring(0,230) + "..." + '</td>'
//                         }
//                         else {
//                             result += hasil.items[i].volumeInfo.description + '</td>'
//                         }
//                     } 
//                     else {
//                         result += "---";
//                     }

//                     if (hasil.items[i].volumeInfo.imageLinks != null) {
//                         result += ('<td> <img src=' + hasil.items[i].volumeInfo.imageLinks.smallThumbnail + '></td>' + '</tr>')
//                     }
//                     else {
//                         result += ('<td> <p> --- </p> </td>' + '</tr>')
//                     }
//                 }
//                 $('#hasil').append(result);
//                 $('#hasil').append('</table>');
//             }
//         })
//     });
// });
// $(document).ready(function() {
//     $("#search").click(function() {
//        var kw = $("#keyword").val();
//         console.log(kw);
//        $.ajax({
//            method : 'GET',
//            url : "/cari-produk",
//            success : function(hasil) {
//                console.log(hasil);
//                $("#result").empty();
//                var result = "";
//                for (let i = 0; i < hasil.items.length; i++) {
//                    result += ('<tr>' + '<td> <a id="judul" href="' + hasil.items[i].volumeInfo.canonicalVolumeLink + '">' + hasil.items[i].volumeInfo.title + ' </a> </td>' + '<td>' + hasil.items[i].volumeInfo.authors + '</td>' + '<td>')

//                    if (hasil.items[i].volumeInfo.description != undefined) {
//                        if (hasil.items[i].volumeInfo.description.length > 230) {
//                            result += hasil.items[i].volumeInfo.description.substring(0,230) + "..." + '</td>'
//                        }
//                        else {
//                            result += hasil.items[i].volumeInfo.description + '</td>'
//                        }
//                    } 
//                    else {
//                        result += "---";
//                    }

//                    if (hasil.items[i].volumeInfo.imageLinks != null) {
//                        result += ('<td> <img src=' + hasil.items[i].volumeInfo.imageLinks.smallThumbnail + '></td>' + '</tr>')
//                    }
//                    else {
//                        result += ('<td> <p> --- </p> </td>' + '</tr>')
//                    }
//                }
//                $('#hasil').append(result);
//                $('#hasil').append('</table>');
//            }
//        })
//    });
// })


var updateBtns = $('.update-cart')
console.log(updateBtns)
for (var i = 0; i < updateBtns.length; i++) {
    $(updateBtns[i]).click(
        function(e) {
            var prodId = this.dataset.product
            var action = this.dataset.action
            console.log("prodId:", prodId, "action:", action)

            console.log("USER:", user)
            if (user === 'AnonymousUser') {
                console.log("User is not authenticated")
            }
            else {
                updateUserOrder(prodId, action)
            }
        }
    )   
}
function updateUserOrder(prodId, action){
    console.log("User is authenticated, sending data...") 
    var url = "/updateitem/"

    fetch(url, {
        method: 'POST',
        headers: {
            'Content-Type':'application/json',
            'X-CSRFToken':csrftoken,
        },
        body: JSON.stringify({'prodId':prodId, 'action':action})
    })
    .then((response) =>{
        return response.json()
    })
    .then((data) =>{
        console.log('data:', data)
        location.reload()
    })
}

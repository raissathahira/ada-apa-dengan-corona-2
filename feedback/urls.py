from django.urls import path
from . import views
from django.contrib import admin

app_name = 'feedback'

urlpatterns = [
    path('feedback/', views.home, name='home'),
    path('pengalaman/', views.experience, name='pengalaman'),
    path('masalah/', views.prob, name='masalah'),
    path('save/', views.thanks, name='save'),
]
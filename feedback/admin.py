from django.contrib import admin
from .models import exp, problem


admin.site.site_header = "Admin Dashboard"
# Register your models here.
@admin.register(exp)
class expfeedback(admin.ModelAdmin):
    list_display = ('nama', 'pesan')

@admin.register(problem)
class probsfeedback(admin.ModelAdmin):
    list_display = ('nama', 'masalah')




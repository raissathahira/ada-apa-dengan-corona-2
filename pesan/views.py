from django.shortcuts import render, redirect
from .forms import pesanform
from .models import expu

from django.http import HttpResponseRedirect,JsonResponse
import json
# Create your views here.
def home(request):
    return render(request, 'basic.html')
    
def experience(request):
    if request.method == "POST":
        form = pesanform(request.POST)
        if (form.is_valid()):
            inp = expu()
            inp.nama = form.cleaned_data['nama']
            inp.pesan = form.cleaned_data['pesan']
            inp.save()
            return HttpResponseRedirect('/')
        else:
            stud = expu.objects.all()
    form = pesanform()
    return render(request, 'basic.html', {'form': form})

def hasil(request):
    inp = expu.objects.all()
    response = {"inp":inp}
    return render(request,'result.html',response)

def hasil2(request):
    inp = expu.objects.all()
    response = {"inp":inp}
    return render(request,'result2.html',response)

def search(request):
    searchexpu = request.GET['q']
    objexpu = expu.objects.filter(nama__icontains=searchexpu)
    data = objexpu.values()
    return JsonResponse(list(data), safe=False)
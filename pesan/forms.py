from django.core import validators
from django import  forms 
from django.forms import Textarea
from .models import expu

class pesanform(forms.ModelForm):
    required_css_class = "required"
    class Meta:
        model = expu
        fields = ['nama','pesan']
        widgets ={
                'nama' : forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Tuliskan namamu disini...'}),
                'pesan' : forms.Textarea(attrs={'class': 'form-control','rows':10, 'cols':10, 'placeholder': 'Tuliskan pesan kamu disini'}),
        }

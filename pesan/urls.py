from django.urls import path
from . import views
from django.contrib import admin

app_name = 'pesan'

urlpatterns = [
    path('pesan/', views.home, name='home'),
    path('datas/', views.experience, name='isi'),
    path('res/', views.hasil, name='res'),
    path('res2/', views.hasil2, name='res2'),
    path('search/', views.search, name='search'),
]
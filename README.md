# Anggota kelompok :
1. Fahira Anya Katili - 1906399770
2. Muhammad Halim Susilo - 1906399796
3. Muhammad Rizki Fauzan - 1906400375
4. Raissa Naila Thahira - 1906400236

# Link Pipeline
[![pipeline status](https://gitlab.com/raissathahira/tk-fauzan-raissa-halim-anya/badges/master/pipeline.svg)](https://gitlab.com/raissathahira/tk-fauzan-raissa-halim-anya/-/commits/master)

# Cerita Aplikasi dan Kebermanfaatannya
Web ini bertujuan untuk memberikan wawasan kepada masyarakat tentang Covid-19 yang berada di Indonesia yang berisi berita dan data. Manfaat dari website ini adalah orang yang mengunjungi website ini akan meneirma informasi tentang Covid-19 dan bisa membeli produk yang kami tawarkan,yaitu masker dan hand sanitizer.

# Daftar fitur yang akan diimplementasikan
1. Berita
2. Artikel
3. Tombol buy yang akan redirect ke Shopee atau Tokopedia sesuai dengan produk yang dipilih pengunjung website.

# LINK HEROKUAPP
ada-apa-dengan-corona.herokuapp.com

# LINK FIGMA
https://www.figma.com/file/9yAXHn5LvIM6hatkrL8ooj/TK-PPW-YOK-BISA?node-id=0%3A1

from . import views
from .views import inF, outF, register
from django.contrib.auth.models import User
from django.urls import reverse, resolve
from django.test import LiveServerTestCase, TestCase, tag, Client
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm

# Create your tests here.
class authenticationUnitTest(TestCase):
    def test_url_login(self):
        response = Client().get("/login")
        self.assertEquals(response.status_code, 200)

    def test_template_login(self):
        response = Client().get("/login")
        self.assertTemplateUsed(response, "login.html")
    
    def test_template_regist(self):
        response = Client().get("/regist")
        self.assertTemplateUsed(response, "regist.html")
    
    def test_url_logout(self):
        response = Client().get("/logout")
        self.assertEquals(response.status_code, 302)
    
    def test_url_regist(self):
        response = Client().get("/regist")
        self.assertEquals(response.status_code, 200)
    
    def test_form_validation_accepted(self):
        self.credentials = {
            'username': 'hihihihi',
            'password': 'hehe1234'}
        User.objects.create_user(**self.credentials)
        form = AuthenticationForm(data=self.credentials)
        self.assertTrue(form.is_valid())

    def test_register_url_exist_view_template_used(self):
        response = Client().get("/regist")
        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response, "regist.html")
    

from django.shortcuts import render, redirect
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm
from django.contrib.auth import login, logout
from django.contrib.auth.decorators import login_required



# Create your views here.

def register(request):
    if request.method == "POST":
        form = UserCreationForm(data=request.POST)
        if form.is_valid():
            login(request, form.save())
            return redirect('/')
    else:
        form = UserCreationForm()
    return render (request,'regist.html', {'form': form})

def inF(request):
    if request.method == "POST":
        form = AuthenticationForm(data=request.POST)
        if form.is_valid():
            login(request, form.get_user())
            return redirect('/')
    else:
        form = AuthenticationForm()
    return render(request, "login.html", {'form': form})

def outF(request):
    logout(request)
    return redirect('/')

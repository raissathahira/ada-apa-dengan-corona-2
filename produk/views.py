from django.shortcuts import render, redirect
from django.http import HttpResponse, JsonResponse
from .models import Customer, Produk, Order, Cart
from django.contrib.auth.models import User
import json
from django.core import serializers

# Create your views here.
def produk(request):
    if request.user.is_authenticated:
        customer, created = Customer.objects.get_or_create(user=request.user)
        order, created = Order.objects.get_or_create(customer=customer, complete=False)
        items = order.cart_set.all()
        cartitems = order.get_cart_items
    else:
        items = []
        order = {'get_cart_total':0, 'get_cart_items':0}
        cartitems = order['get_cart_items']
    produk = Produk.objects.all()
    return render(request, 'produk.html', {'produk':produk, 'cartitems':cartitems})

def cart(request):
    if request.user.is_authenticated:
        customer, created = Customer.objects.get_or_create(user=request.user)
        order, created = Order.objects.get_or_create(customer=customer, complete=False)
        items = order.cart_set.all()
    else:
        items = []
        order = {'get_cart_total':0}
    return render(request, 'cart.html', {'items':items, 'order':order})

def createadmin(request):
    if not User.objects.filter(username="admin").exists():
        user = User.objects.create_user(username="admin", password="aadc", first_name="ada apa", last_name="dengan corona")
        user.is_superuser = True
        user.is_staff = True
        user.save()
        return redirect("../")
    else:
        return redirect("../")

def updateitem(request):
    data = json.loads(request.body)
    prodId = data['prodId']
    action = data['action']

    print("Action: ", action)
    print("Product ID: ", prodId )

    customer = request.user.customer
    product = Produk.objects.get(id=prodId)
    order, created = Order.objects.get_or_create(customer=customer, complete=False)

    cart, created = Cart.objects.get_or_create(order=order,product=product)

    if action == "add":
        cart.quantity += 1
    elif action == 'remove':
        cart.quantity -= 1

    cart.save()

    if action == 'delete' or cart.quantity <= 0:
        cart.delete()

    print(cart.quantity)
    return JsonResponse('Item was added', safe=False)

def checkout(request):
    Cart.objects.all().delete()
    return render(request, "checkout.html")

def find_produk(request):
    produk = Produk.objects.all()
    json_data_produk = serializers.serialize("json", produk)
    return HttpResponse(json_data_produk, content_type="application/json")

from django.urls import path
from . import views

app_name = "produk"

urlpatterns = [
    path('produk/', views.produk, name='produk'),
    path('cart/', views.cart, name='cart'),
    path('createadmin/', views.createadmin, name='createadmin'),
    path('updateitem/', views.updateitem, name='updateitem'),
    path('checkout/', views.checkout, name='checkout'),
    path('cari-produk/', views.find_produk),
]
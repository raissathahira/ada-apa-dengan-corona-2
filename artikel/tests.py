from django.test import TestCase, Client
from django.urls import resolve
from .views import formCom
from .models import Post, Comment

# Create your tests here.
class TestIndexArtikel(TestCase):
    def test_url_tersedia(self):
        response = Client().get('/artikel/')
        self.assertEquals(response.status_code, 200)
    def test_memakai_template(self):
        response = Client().get('/artikel/')
        self.assertTemplateUsed(response, 'artindex.html')
    def test_artikel_muncul(self):
        artikel = Post(title='Ini Test', slug='ini-test', author='Dadang',
        content='ini test', status='1')
        artikel.save()
        self.assertEquals(Post.objects.all().count(), 1)
    
class TestPostDetail(TestCase):
    def setUp(self):
        artikel = Post(title='Ini Test', slug='ini-test', author='Dadang',
        content='ini test', status='1')
        artikel.save()
    def test_memakai_template(self):
        response = Client().get('/artikel/%s/' % (Post.objects.get(slug='ini-test').slug))
        self.assertTemplateUsed(response, 'post_detail.html')
    def test_tambahpst_POST(self):
        response =  Client().post(('/artikel/%s/' % (Post.objects.get(slug='ini-test').slug)), 
                    data={'uname': 'Mang Oleh', 'com': 'Rasanya Anjayani!'})
        self.assertEquals(response.status_code, 302)
    def test_tambahpst_GET(self):
        response = self.client.get('/artikel/%s/' % (Post.objects.get(slug='ini-test').slug))
        self.assertTemplateUsed(response, 'post_detail.html')
        self.assertEqual(response.status_code, 200)

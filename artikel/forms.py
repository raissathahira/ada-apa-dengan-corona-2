from django import forms
from .models import Comment

class Form_Comment(forms.ModelForm):
    class Meta:
        model = Comment
        fields = ['uname', 'com']

    error_messages = {
        'required' : 'Tolong diisi'
    }
    
    uname_attrs = {
        'type':'text',
        'class':"form-control",
        'placeholder':'Username'
    }
    
    com_attrs = {
        'type':'text',
        'class':"form-control",
        'placeholder':'Tuliskan Komentarmu Disini!'
    }

    uname = forms.CharField(label='', required=True, max_length=30, widget=forms.TextInput(attrs=uname_attrs))
    com = forms.CharField(label='', required=True, max_length=200, widget=forms.Textarea(attrs=com_attrs))